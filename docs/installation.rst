.. highlight:: shell

============
Installation
============


Stable release
--------------

To install Example GitLab Python Project, run this command in your terminal:

.. code-block:: console

    $ pip install example_gitlab_python_project

This is the preferred method to install Example GitLab Python Project, as it will always install the most recent stable release.

If you don't have `pip`_ installed, this `Python installation guide`_ can guide
you through the process.

.. _pip: https://pip.pypa.io
.. _Python installation guide: http://docs.python-guide.org/en/latest/starting/installation/


From sources
------------

The sources for Example GitLab Python Project can be downloaded from the `Github repo`_.

You can either clone the public repository:

.. code-block:: console

    $ git clone git://gitlab.com/AdriaanRol/example_gitlab_python_project

Or download the `tarball`_:

.. code-block:: console

    $ curl -OJL https://gitlab.com/AdriaanRol/example-gitlab-python-project/tarball/master

Once you have a copy of the source, you can install it with:

.. code-block:: console

    $ python setup.py install


.. _Gitlab repo: https://gitlab.com/AdriaanRol/example-gitlab-python-project
.. _tarball: https://gitlab.com/AdriaanRol/example-gitlab-python-project/tarball/master
